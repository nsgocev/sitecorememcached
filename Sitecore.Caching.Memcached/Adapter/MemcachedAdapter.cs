﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;
using Sitecore.Caching.Generics;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Diagnostics.PerformanceCounters;

namespace Sitecore.Caching.Memcached.Adapter
{
    public class MemcachedAdapter : ICache
    {
        private readonly object _syncRoot = new object();
        private readonly string _name;
        private readonly IMemcachedClient _memcachedClient;
        private readonly ICollection<IPEndPoint> _serverIpEndpoints;
        

        private volatile ICacheSizeCalculationStrategy _strategy;

        private readonly ID _id = ID.NewID;
        private bool _enabled;

        private Dictionary<string, string> _cacheKeys;
    

        public MemcachedAdapter(string name, string maxSize)
            : this(name, StringUtil.ParseSizeString(maxSize))
        {
        }

        public MemcachedAdapter(string name, long maxSize)
            : this(name, maxSize, true)
        {
        }

        public MemcachedAdapter(string name, long maxSize, bool register)
        {
            Assert.ArgumentNotNull(name, "name");
            _name = name;

            int maxCacheSize = (int)(maxSize / 1024.0 / 1024.0);

            if (maxCacheSize == 0 && maxSize != 0L)
            {
                maxCacheSize = 1;
            }
          
            if (!register)
            {
                return;
            }

            _serverIpEndpoints = new List<IPEndPoint>
            {
                new IPEndPoint(IPAddress.Parse("127.0.0.1"), 11211)
            };

            IMemcachedClientConfiguration config = new MemcachedClientConfiguration();

            foreach (IPEndPoint endpoint in _serverIpEndpoints)
            {
                config.Servers.Add(endpoint);
            }

            _memcachedClient = new MemcachedClient(config);

            _enabled = Settings.Caching.Enabled && maxCacheSize > 0L;
           
            _cacheKeys = new Dictionary<string, string>();
            
            CacheManager.Register(this);
        }

        public object this[string key]
        {
            get { return _memcachedClient.Get(FormatKeyNameForMemCached(key)); }
        }

        public ICacheSizeCalculationStrategy CacheSizeCalculationStrategy
        {
            get
            {
                if (_strategy == null)
                {
                    lock (_syncRoot)
                    {
                        if (_strategy == null)
                        {
                            _strategy = new DefaultCacheSizeCalculationStrategy();
                        }
                    }
                }
                return _strategy;
            }
            protected set
            {
                _strategy = value;
            }
        }

        public int Count
        {
            get
            {
                int totalItems = 0;

                foreach (IPEndPoint endpoint in _serverIpEndpoints)
                {
                    string serverCount = _memcachedClient.Stats().GetRaw(endpoint, StatItem.ItemCount);

                    int serverCountInt;

                    if (int.TryParse(serverCount, out serverCountInt))
                    {
                        totalItems += serverCountInt;
                    }
                }

                return totalItems;
            }
        }

        public bool Enabled
        {
            get
            {
                if (_enabled)
                {
                    return MaxSize > 0L;
                }

                return false;
            }
            set
            {
                _enabled = value;
            }
        }

        public AmountPerSecondCounter ExternalCacheClearingsCounter
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public ID Id
        {
            get { return _id; }
        }

        public long MaxSize
        {
            get
            {
                long maxBytes = 0;

                foreach (IPEndPoint endpoint in _serverIpEndpoints)
                {
                    maxBytes += _memcachedClient.Stats().GetValue(endpoint, StatItem.MaxBytes);
                }

                return maxBytes;

            }
            set { throw new NotSupportedException("Memcached Decided how much bytes you see. Not you !"); }
        }

        public string Name
        {
            get { return _name; }
        }

        public long RemainingSpace
        {
            get { return MaxSize - Size; }
        }

        public bool Scavengable
        {
            get
            {
                return true;
            }
            set
            {
                Log.SingleWarn("MemcachedAdapter does not support changing property 'Scavengable'.", this);
            }
        }

        public long Size
        {
            get
            {
                long maxBytes = 0;

                foreach (IPEndPoint endpoint in _serverIpEndpoints)
                {
                    maxBytes += _memcachedClient.Stats().GetValue(endpoint, StatItem.UsedBytes);
                }

                return maxBytes;
            }
        }

        public void Add(string key, object data)
        {
            Assert.ArgumentNotNull(key, "key");
            Assert.ArgumentNotNull(data, "data");

            if (!Enabled)
            {
                return;
            }

            string formattedKey = FormatKeyNameForMemCached(key);

            AddCacheKey(key, formattedKey);
            _memcachedClient.Store(StoreMode.Add, formattedKey, data);
        }

        public void Add(string key, object data, DateTime absoluteExpiration)
        {
            Assert.ArgumentNotNull(key, "key");
            Assert.ArgumentNotNull(data, "data");

            if (!Enabled)
            {
                return;
            }

            string formattedKey = FormatKeyNameForMemCached(key);

            AddCacheKey(key, formattedKey);

            _memcachedClient.Store(StoreMode.Add, formattedKey, data, absoluteExpiration);
        }

        public void Add(string key, object data, EventHandler<EntryRemovedEventArgs<string>> removedHandler)
        {
            throw new NotSupportedException("Memcached doesn`t support removed handlers");
        }

        public void Add(string key, object data, TimeSpan slidingExpiration)
        {
            Assert.ArgumentNotNull(key, "key");
            Assert.ArgumentNotNull(data, "data");

            key = FormatKeyNameForMemCached(key);

            if (!Enabled)
            {
                return;
            }

            string formattedKey = FormatKeyNameForMemCached(key);

            AddCacheKey(key, formattedKey);

            _memcachedClient.Store(StoreMode.Add, formattedKey, data, slidingExpiration);
           }

        public void Add(string key, object data, TimeSpan slidingExpiration, DateTime absoluteExpiration)
        {
            Assert.ArgumentNotNull(key, "key");
            Assert.ArgumentNotNull(data, "data");

            key = FormatKeyNameForMemCached(key);

            if (!Enabled)
            {
                return;
            }

            string formattedKey = FormatKeyNameForMemCached(key);

            AddCacheKey(key, formattedKey);

            if (slidingExpiration == CacheManager.NoSlidingExpiration)
            {
                Add(formattedKey, data, absoluteExpiration);
            }

            Add(formattedKey, data, slidingExpiration);
        }

        public void Add(string key, object data, TimeSpan slidingExpiration, DateTime absoluteExpiration,
            EventHandler<EntryRemovedEventArgs<string>> removedHandler)
        {
            throw new NotSupportedException("Memcached doesn`t support removed handlers");
        }
    

        public void Clear()
        {
            ClearCacheKeys();
           _memcachedClient.FlushAll();
        }

        public bool ContainsKey(string key)
        {
            return _cacheKeys.ContainsKey(key);
        }

        public string[] GetCacheKeys()
        {
            return _cacheKeys.Keys.ToArray();
        }

        public object GetValue(string key)
        {
            key = FormatKeyNameForMemCached(key);
            return _memcachedClient.Get(key);
        }

        public void Remove(string key)
        {
            RemoveCacheKey(key);
            _memcachedClient.Remove(FormatKeyNameForMemCached(key));
        }

        public ICollection<string> Remove(Predicate<string> predicate)
        {
            Assert.ArgumentNotNull(predicate, "predicate");
            List<string> stringList = new List<string>();

            foreach (string cacheKey in GetCacheKeys())
            {
                if (predicate(cacheKey))
                {
                    Remove(cacheKey);
                    stringList.Add(cacheKey);
                }
            }

            return stringList;

        }

        public void RemoveKeysContaining(string keyPart)
        {
            foreach (string cacheKey in GetCacheKeys())
            {
                if (cacheKey.IndexOf(keyPart, StringComparison.InvariantCultureIgnoreCase) > -1)
                {
                    Remove(cacheKey);
                }
            }
        }

        public void RemovePrefix(string prefix)
        {
            foreach (string cacheKey in GetCacheKeys())
            {
                if (cacheKey.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase))
                {
                    Remove(cacheKey);
                }
            }
        }

        public void Scavenge()
        {
           
        }

        private string FormatKeyNameForMemCached(string key)
        {
            return Regex.Replace(key, @"[^0-9a-zA-Z]+", string.Empty);
        }

        private void AddCacheKey(string key, string formatedKey)
        {
            _cacheKeys.Add(key, formatedKey);
        }

        private void RemoveCacheKey(string key)
        {
            _cacheKeys.Remove(key);
        }

        private void ClearCacheKeys()
        {
            _cacheKeys = new Dictionary<string, string>();
        }

    }
}